#!/usr/bin/make -f
## ----------------------------------------------------------------------
## debian/rules : package script for debian-reference
## ----------------------------------------------------------------------


## ----------------------------------------------------------------------
## uncomment this to turn on verbose mode
#export DH_VERBOSE=1
## no-parallel to build with the cleaner log (it slows!)
#BUILDOPT:=	 --parallel
#BUILDOPT:=	 --no-parallel

## ----------------------------------------------------------------------
# Package building language choice
# (This should be updated with the maintainer approval)
#
MANUAL	:=	debian-reference
LANGALL	:=	en ja fr id it pt de zh-cn zh-tw es pt-br
LANGPO	:=	   ja fr id it pt de zh-cn zh-tw es pt-br
# languages to skip generation of PDF files (not used now)
#NOPDF	:=	zh-cn zh-tw
NOPDF	:=

## ----------------------------------------------------------------------
## Targets

%:
	dh $@

override_dh_auto_build:
	# Generate files for debhelper in debian/
	cd $(CURDIR)/debian/ ; \
	cp all.postinst-in  $(MANUAL)-common.postinst ; \
	cp all.postrm-in    $(MANUAL)-common.postrm ; \
	for XX in $(LANGALL) ; do \
	  cp all.postinst-in  $(MANUAL)-$${XX}.postinst ; \
	  cp all.postrm-in    $(MANUAL)-$${XX}.postrm ; \
	  case $$XX in \
	    en) LANGX="English" ;;  \
	    de) LANGX="German" ;;   \
	    fr) LANGX="French" ;;   \
	    fi) LANGX="Finnish" ;;  \
	    es) LANGX="Spanish" ;;  \
	    id) LANGX="Indonesian" ;;  \
	    it) LANGX="Italian" ;;  \
	    ja) LANGX="Japanese" ;; \
	    pl) LANGX="Polish" ;; \
	    pt) LANGX="Portuguese (Portugal)" ;; \
	    pt-br) LANGX="Portuguese (Brazil)" ;; \
	    zh-tw) LANGX="Chinese (Traditional)" ;; \
	    zh-cn) LANGX="Chinese (Simplified)" ;; \
	    *) LANGX="$$x" ;; \
	  esac ; \
	  sed  -e "s/@@/$$XX/g" < all.install-in  >$(MANUAL)-$${XX}.install ; \
	  sed  -e "s/@@/$$XX/g" < all.links-in  >$(MANUAL)-$${XX}.links ; \
	  sed  -e "s/@@/$$XX/g" -e "s/@LANG@/$$LANGX/g" < all.doc-base-in  >$(MANUAL)-$${XX}.doc-base ; \
	done
	dh_auto_build $(BUILDOPT) -- \
		"LANGALL=$(LANGALL)" \
		"LANGPO=$(LANGPO)" \
		"NOPDF=$(NOPDF)" \
		all


override_dh_compress:
	dh_compress -X.pdf

override_dh_auto_test:
	# just in case backported to debhelper pre-13
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# error if shipped scripts have syntax error in them
	sh -n bin/debian-reference
	sh -n bin/mkindexhtml
endif

override_dh_auto_clean:
	# Clean files for debhelper in debian/
	-rm -f $(MANUAL)-common.postinst $(MANUAL)-common.postrm
	-rm -f $(addsuffix .postinst, $(addprefix  debian/$(MANUAL)-, $(LANGALL) common))
	-rm -f $(addsuffix .postrm,   $(addprefix  debian/$(MANUAL)-, $(LANGALL) common))
	-rm -f $(addsuffix .links,    $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .install,  $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	-rm -f $(addsuffix .doc-base, $(addprefix  debian/$(MANUAL)-, $(LANGALL)))
	$(MAKE) "LANGALL=$(LANGALL)" "LANGPO=$(LANGPO)"  clean
