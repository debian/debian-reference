<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>
<!-- vim: set sts=2 ai expandtab: -->

<!--############################################################################
    XSLT Stylesheet DocBook -> LaTeX using sans fonts
    ############################################################################ -->

  <xsl:param name="xetex.font">
    <!-- Workaround to disable curved quotation mark in listing env -->
    <xsl:text>\usepackage{listings}&#10;</xsl:text>
    <xsl:text>\lstset{upquote=true,basicstyle=\ttfamily}&#10;</xsl:text>
    <xsl:text>\defaultfontfeatures{}&#10;</xsl:text>

    <!-- Western centric Adobe PostScript looks-alike: gsfonts: missing some odd codes, mono is thin
    <xsl:text>\setmainfont{Nimbus Roman No9 L}&#10;</xsl:text>
    <xsl:text>\setsansfont{Nimbus Sans L}&#10;</xsl:text>
    <xsl:text>\setmonofont{Nimbus Mono L}&#10;</xsl:text>
    -->

    <!-- Western centric Bitstream Vera Fonts extended: DejaVu: too big and spaced
    <xsl:text>\setmainfont{DejaVu Serif}&#10;</xsl:text>
    <xsl:text>\setsansfont{DejaVu Sans}&#10;</xsl:text>
    <xsl:text>\setmonofont{DejaVu Sans Mono}&#10;</xsl:text>
    -->

    <!-- Western centric MS (monotype) fonts looks-alike: Liberation : Best looking -->
    <xsl:text>\setmainfont{Liberation Sans}&#10;</xsl:text>
    <xsl:text>\setsansfont{Liberation Sans}&#10;</xsl:text>
    <xsl:text>\setmonofont{Liberation Mono}&#10;</xsl:text>

    <xsl:choose>
    <!-- TODO: perhaps use fonts-noto-cjk for all -->
      <xsl:when test="$lingua = 'zh-cn'">
    <!-- zh_CN centric: fonts-wqy-zenhei -->
        <xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
        <xsl:text>\setCJKmainfont{WenQuanYi Zen Hei}&#10;</xsl:text>
        <xsl:text>\setCJKsansfont{WenQuanYi Zen Hei}&#10;</xsl:text>
        <xsl:text>\setCJKmonofont{WenQuanYi Zen Hei Mono}&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="$lingua = 'zh-tw'">
        <!-- zh_TW centric: fonts-wqy-zenhei -->
        <xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
        <xsl:text>\setCJKmainfont{WenQuanYi Zen Hei}&#10;</xsl:text>
        <xsl:text>\setCJKsansfont{WenQuanYi Zen Hei}&#10;</xsl:text>
        <xsl:text>\setCJKmonofont{WenQuanYi Zen Hei Mono}&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="$lingua = 'ja'">
        <!-- ja centric: fonts-vlgothic -->
        <xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
        <xsl:text>\setCJKmainfont{VL PGothic}&#10;</xsl:text>
        <xsl:text>\setCJKsansfont{VL PGothic}&#10;</xsl:text>
        <xsl:text>\setCJKmonofont{VL Gothic}&#10;</xsl:text>
      </xsl:when>
      <xsl:when test="$lingua = 'ko'">
        <!-- ko centric: fonts-unfonts-core -->
        <xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
        <xsl:text>\setCJKmainfont{UnDotum}&#10;</xsl:text>
        <xsl:text>\setCJKsansfont{UnDotum}&#10;</xsl:text>
        <xsl:text>\setCJKmonofont{UnDotum}&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <!-- only western char -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:param>

</xsl:stylesheet>

