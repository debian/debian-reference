#######################################################################
# Build debian-reference (v2)
# vim: set ts=8:
#######################################################################
#======================================================================
# ===== Adjustable parameters =========================================
#======================================================================
# base file name excluding file extension
MANUAL	:=	debian-reference
#
# build strategy depends on build environment (robust build)
#STRATEGY := $(shell if [ "$$(lsb_release -s -r)" = "n/a" ]; then echo "unstable"; else echo "stable" ; fi)
# When texlive tool chain suffer problem, run "make prep" and commit "keep/"
# directoru in stable environment and build package in STRATEGY=unstable
# environment
STRATEGY := stable
# When OK situation, uncomment to set as above

# languages translated with PO files (Below are as of 2024-05-28)
# ja  100 %       -- COMPLETED translation (must be more than 40%)
# fr  100 %       -- COMPLETED translation (must be more than 40%)
# it  100 %       -- COMPLETED translation (must be more than 40%)
# es  100 %       -- COMPLETED translation (must be more than 40%)
# zh-cn  100 %       -- COMPLETED translation (must be more than 40%)
# id  96 %       -- COMPLETED translation (must be more than 40%)
# de  94 %       -- COMPLETED translation (must be more than 40%)
# pt-br  79 %       -- COMPLETED translation (must be more than 40%)
# pt  78 %       -- COMPLETED translation (must be more than 40%)
# zh-tw  74 %       -- COMPLETED translation (must be more than 40%)
# nb  6 %       -- COMPLETED translation (must be more than 40%)
# sv  11 %       -- COMPLETED translation (must be more than 40%)
LANGPO		:=	ja fr it es zh-cn id de pt-br pt zh-tw nb sv
LANGPO_PREP	:=	ja fr it es zh-cn id de pt-br pt zh-tw
# ko fi are less than 10 strings -- skipped even for make
#LANGPO	:=	ja fr it es zh-cn id de pt-br pt zh-tw nb sv ko fi
# languages to skip generation of PDF files (not used now)

NOPDF	:=
# languages to build document
LANGALL	=	en $(LANGPO)
# Packages uploaded to unstable repo generate official Debian web pages
# Change these to post-release state when Debian is under soft freeze
#
# current "stable" release name
RELEASE_NAME0L := bookworm
RELEASE_NAME0U := Bookworm
# next "stable" / "testing" release name
RELEASE_NAME1L := trixie
RELEASE_NAME1U := Trixie
# next next "stable" / next "testing" release name
RELEASE_NAME2L := forky
RELEASE_NAME2U := Forky
# release name considered outdated (Release_Date-EOL_for_LTS)
RELEASE_NAMEXX := Jessie 8 (2015-2020)
# release names when this document was last updated
RELEASE_DOC_1  := Bookworm (=<literal>stable</literal>)
RELEASE_DOC_2  := Trixie (=<literal>testing</literal>)

# version in changelog
DR_VERSION :=	$(shell dpkg-parsechangelog --show-field Version)

# set SOURCE_DATE_EPOCH if unset (when not called from debhelper)
SOURCE_DATE_EPOCH ?= $(shell dpkg-parsechangelog -STimestamp)
# Export for reproducible PDF build
#  2020: https://tex.stackexchange.com/questions/568235/how-do-i-configure-pdflatex-to-output-binary-equivalent-pdfs-if-the-input-tex-fi
#  2015: https://tex.stackexchange.com/questions/229605/reproducible-latex-builds-compile-to-a-file-which-always-hashes-to-the-same-va
export SOURCE_DATE_EPOCH
FORCE_SOURCE_DATE = 1
export FORCE_SOURCE_DATE

# date in changelog
SOURCE_DATE := $(shell date -u +'%F %T %Z' --date=@$(SOURCE_DATE_EPOCH))

# BUILD_DIR is not mean to be set to generic TMPDIR
BUILD_DIR	:= $(CURDIR)/build
# KEEP_DIR holds PDFs build in the stable environment
KEEP_DIR	:= $(CURDIR)/keep

# Change $(DRAFTMODE) from "yes" to "maybe" when this document
# should go into production mode
#DRAFTMODE      := yes
DRAFTMODE       := maybe
export DRAFTMODE

# define EXPOSEMODE to yes to expose not-so-popular program listings in table list
# (This is used to hide marginal table list entry.)
EXPOSEMODE ?= no
ifeq "$(EXPOSEMODE)" "yes"
XML_COMMENT_START=
XML_COMMENT_END=
else
XML_COMMENT_START= <!--
XML_COMMENT_END= -->
endif

# Prevent external environment vriables to impact build result
TZ	:= "0"
export TZ
# Reset all locale variables for clean perl execution
unexport LANG
unexport LANGUAGE
unexport LC_TIME
unexport LC_COLLATE
unexport LC_ALL
unexport LC_MEASUREMENT
unexport LC_NUMERIC
unexport LC_PAPER
unexport LC_MONETARY

# for reproducible build
FORCE_SOURCE_DATE = 1
export FORCE_SOURCE_DATE
#######################################################################
### basic constant parameters
#######################################################################
# Directories (no trailing slash)
DXSL	:=	xslt
DBIN	:=	bin
DRAW	:=	rawxml
DPO	:=	po
DIMG	:=	/usr/share/xml/docbook/stylesheet/nwalsh/images

# work around https://bugs.debian.org/725931
PERL_PERTURB_KEYS := 0
export PERL_PERTURB_KEYS
PERL_HASH_SEED := 0
export PERL_HASH_SEE

# Program name and option
# stop using faketime
XLINT	:=	xmllint --format
XPNO	:=	xsltproc --novalid --nonet
XPINC	:=	xsltproc --novalid --nonet --xinclude
MSGATTR	:=	msgattrib
MSGCAT	:=	msgcat
MSGUNIQ	:=	msguniq --use-first
DBLATEX	:=	/usr/bin/dblatex
SED	:=	sed
ZIP	:=      zip

# Source files
SRC_XML :=	$(wildcard $(DRAW)/*_*.rawxml)
SRC_ENT:=	remote.ent common.ent
# source PO files for all languages (build process requires these)
SRC_PO :=	$(addsuffix .po, $(addprefix  $(DPO)/, $(LANGPO)))
# sed script for replace
SEDF ?= $(DBIN)/replace0

# po4a/opencc translation
PO4A1 ?= $(DBIN)/po4a1
PO4A2 ?= $(DBIN)/po4a2
PO4ATMP ?= $(DBIN)/po4atmp
POFUZZY1 ?= $(DBIN)/pofuzzy1
POFUZZY2 ?= $(DBIN)/pofuzzy2

# Remote file source (related to Debian package archive URLs)
#DEBM	:=	http://ftp.us.debian.org/debian/dists
#DEBM	:=	http://ftp.jp.debian.org/debian/dists
DEBM	:=	http://deb.debian.org/debian/dists
# Debian popcon data source URL
UPOPC	:=	https://popcon.debian.org/all-popcon-results.txt.gz
# Debian release name and arch used
CODE	:=	sid
ARCH	:=	amd64
UDEBA	:=	$(DEBM)/$(CODE)
UDEBB	:=	$(DEBM)/experimental
UDEBC	:=	$(DEBM)/stable
UDEBD	:=	$(DEBM)/oldstable
# processed remote data
PACKAGES_DATA := packages.main packages.non-free-firmware packages.contrib packages.non-free \
		packages.txt packages.experimental.txt \
		packages.stable.txt packages.oldstable.txt
POPCON_DATA :=  all-popcon-results.txt \
		all-popcon-date.txt \
		all-popcon-submissions.txt \
		all-popcon-pkgs.txt
REMOTE_DATA :=	$(PACKAGES_DATA) $(POPCON_DATA)


#######################################################################
# Used as $(call check-command, <command>, <package>)
define check-command
set -e; if ! type $(1) >/dev/null; then \
  echo "Missing command: $(1), install package: $(2)"; \
  false; \
fi
endef
# Used as $(call check-file, <file>)
define check-file
set -e; if ! [ -r $(1) ]; then \
  echo "Missing file: $(1)"; \
  false; \
fi
endef
#======================================================================
# ===== Utility targets ===============================================
#======================================================================
#######################################################################
# $ make all       # build all formats
#######################################################################
.PHONY: all
# set LANGPO to limit language to speed up build
all: css html txt pdf
	@echo "PO status: fuzzy1 ======================"
	-cat fuzzy1.log
	@echo "PO status: fuzzy2 ======================"
	-cat fuzzy2.log

#######################################################################
# $ make test      # build html for all languages (for Translator)
#######################################################################
.PHONY: test
test: html css
	@echo "PO status: fuzzy1 ======================"
	-cat fuzzy1.log
	@echo "PO status: fuzzy2 ======================"
	-cat fuzzy2.log

#######################################################################
# $ make entity    # refresh remote.ent (Need remote access)
#######################################################################
# For major English update, run this and commit updated data
.PHONY: entity
entity:
	$(MAKE) entityclean
	$(MAKE) remote.ent

#######################################################################
# $ make en      # build html for testing (for English)
#######################################################################
.PHONY: en
en:
	$(MAKE) entity
	$(MAKE) html css LANGPO=

#======================================================================
# ===== Common-build targets (no remote access ) ======================
#======================================================================
$(DRAW)/$(MANUAL).rawxml: $(SRC_XML)
	#echo $^
	cd $(DRAW) ; ./merge.sh

#======================================================================
# ===== Pre-build only (remote access) ================================
#======================================================================
#######################################################################
# Phase 1.1 (pre-build): download Debian stat data (REMOTE_DATA)
#######################################################################
packages.main:
	# FETCH PACKAGE (sid main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.main.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.main.tmp > packages.main
	rm packages.main.tmp

packages.contrib:
	# FETCH PACKAGE (sid contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/contrib/binary-$(ARCH)/Packages.xz   | xzcat - > packages.contrib.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.contrib.tmp > packages.contrib
	rm packages.contrib.tmp

packages.non-free-firmware:
	# FETCH PACKAGE (sid non-free-firmware)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/non-free-firmware/binary-$(ARCH)/Packages.xz  | xzcat - > packages.non-free-firmware.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.non-free-firmware.tmp > packages.non-free-firmware
	rm packages.non-free-firmware.tmp

packages.non-free:
	# FETCH PACKAGE (sid non-free)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/non-free/binary-$(ARCH)/Packages.xz  | xzcat - > packages.non-free.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.non-free.tmp > packages.non-free
	rm packages.non-free.tmp

packages.experimental.txt:
	# FETCH PACKAGE (experimental main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBB)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.experimental.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.experimental.tmp > packages.experimental.txt
	rm packages.experimental.tmp

packages.txt: packages.main packages.non-free-firmware packages.contrib packages.non-free
	# FETCH PACKAGE (sid)
	cat packages.main packages.non-free-firmware packages.contrib packages.non-free >packages.txt

packages.stable.txt:
	# FETCH PACKAGE (stable main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBC)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.stable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.stable.tmp > packages.stable.txt
	rm packages.stable.tmp

packages.oldstable.txt:
	# FETCH PACKAGE (oldstable main contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBD)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp > packages.oldstable.txt
	rm packages.oldstable.tmp
	wget -O - $(UDEBD)/contrib/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp >> packages.oldstable.txt
	rm packages.oldstable.tmp

all-popcon-results.txt:
	# POPCON RESULTS
	wget -O - $(UPOPC) | zcat - > all-popcon-results.txt

all-popcon-submissions.txt: all-popcon-results.txt
	sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt > all-popcon-submissions.txt

all-popcon-date.txt: all-popcon-results.txt
	echo "<!ENTITY pop-date \"$(shell date -u +'%F %T %Z')\">"	>  all-popcon-date.txt

all-popcon-pkgs.txt: all-popcon-results.txt
	grep --binary-files=text -e '^Package: [a-z0-9][-+a-z0-9.]*[ 0-9]*$$' < all-popcon-results.txt > all-popcon-pkgs.txt

# download base data for package data and popcon data
.PHONY: remote
remote: $(REMOTE_DATA)

#######################################################################
# Phase 1.2 (pre-build): extract package name list (pkg.lst)
#######################################################################

# extract package name (@emacs@ etc.) list from RAWXML
# used pre-build time only
pkg.lst: $(DRAW)/$(MANUAL).rawxml
	@$(call check-command, xsltproc, xsltproc)
	# PACKAGE LIST of packages mentioned in the source XML
	$(XPNO) $(DXSL)/pkg.xsl $(DRAW)/$(MANUAL).rawxml > $@

#######################################################################
# Phase 1.3 (pre-build): update Debian archive stat data (remote.ent)
#######################################################################

# source XML inclusion file (pre-commited since this needs remote access)
# no dependency claimed to avoid remote access to update data
remote.ent:
	$(MAKE) $(REMOTE_DATA) pkg.lst
	cat all-popcon-date.txt													>  $@
	echo "<!ENTITY all-packages \"$$(grep -e '^Package:' packages.txt | wc -l)\">"						>> $@
	echo "<!ENTITY main-packages \"$$( grep -e '^Package:' packages.main | wc -l)\">"					>> $@
	echo "<!ENTITY contrib-packages \"$$(grep -e '^Package:' packages.contrib | wc -l)\">"					>> $@
	echo "<!ENTITY non-free-firmware-packages \"$$(grep -e '^Package:' packages.non-free-firmware | wc -l)\">"		>> $@
	echo "<!ENTITY non-free-packages \"$$(grep -e '^Package:' packages.non-free | wc -l)\">"				>> $@
	sort pkg.lst | uniq | $(DBIN)/sizeent packages.txt packages.experimental.txt packages.stable.txt packages.oldstable.txt	>> $@
	echo "<!ENTITY pop-submissions \"$$(sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt)\">"	>> $@
	echo "<!ENTITY pop-architectures \"$$(grep --binary-files=text -e '^Architecture:' all-popcon-results.txt | wc -l)\">"	>> $@
	echo "<!ENTITY pop-packages \"$$(grep -e '^Package:' all-popcon-pkgs.txt | wc -l)\">"					>> $@
	grep -e '^Package:' all-popcon-pkgs.txt | grep -f pkg.lst | $(DBIN)/popconent `cat all-popcon-submissions.txt`		>> $@

#======================================================================
# ===== Main-build only (no remote access) ============================
#======================================================================
#######################################################################
# Phase 2(build): RAWXML (template) -> English XML
#######################################################################

# Make updated URL list for header from $(MANUAL).rawxml
#  Sanity check of URLS
#  - Check duplicates of id with different URLs
#  - Check site URL is active or not
#  - Basically always use URL with https://...
#    - Exceptions:
#      - APT source URLs
#      - http://localhost
#
#  Translate URL for en.wikipedia.org only
#
#  Minimize use of sourceforge.net and sf.net
#
$(DRAW)/headerurl.rawxml: $(DRAW)/$(MANUAL).rawxml
	$(XPNO) $(DXSL)/urls.xsl $(DRAW)/$(MANUAL).rawxml | sort | uniq | sed -e 's/&/\&amp;/g' > $@
	# sanity check source for URL links (duplicates)
	sed -e 's,<!ENTITY \([^ ]*\) .*$$, \1 ,' < $@ | uniq -d > $@.dups
	# sanity check source for URL links (non-HTTPS)
	fgrep -e ' "http://' < $@ | sort | fgrep -v \
		-e 'http://localhost' \
		-e 'http://deb.debian.org' \
		-e 'http://security.debian.org' \
		-e 'http://archive.ubuntu.com' \
		> $@.http || true
	# prep for sanity check source for URL links (non-reachable HTTPS)
	fgrep -e ' "https://' < $@ | sed -e 's,<!ENTITY [^"]*"\([^"]*\)".*$$,\1,' < $@ | sort | uniq > $@.urls
	#
	@if [ "$$(wc -l < $@.dups)" -gt 0 ]; then \
		echo ; echo ">>> W: Duplicate id with different URLs found. ($@.entdups) ." ; \
		grep -f "$@.dups" < $@ | tee $@.entdups; \
		echo ; fi
	@if [ "$$(wc -l < $@.http)" -gt 0 ]; then \
		echo ; echo ">>> W: Non-HTTPS URL links found. Check HTTPS-URLs works or not. ($@.https)" ; \
		sed -e 's/^[^"]*"http/https/' -e 's/"[^"]*$$//' $@.http | sort | tee $@.https ; \
		echo ; fi
	@if grep -e '<!ENTITY  "">' < $@ >/dev/null ; then echo ; \
		echo ">>> W: empty entity value found.  Check sources in $(DRAW)/ ." ; \
		echo ; fi

# download base data for package data and popcon data
.PHONY: url
url: $(DRAW)/headerurl.rawxml

# slow, remote access and check URL link
.PHONY: badurl
badurl:
	$(MAKE) clean
	$(MAKE) url
	: > $(DRAW)/headerurl.rawxml.urlsbad
	# check accessibility of URL: '--head' or '-r 0-0'
	@for f in $$(cat $(DRAW)/headerurl.rawxml.urls) ; do \
		if curl --user-agent "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36" \
		    --output /dev/null --connect-timeout 15 --silent --head --fail $$f ; then \
		  echo "I: good URL $$f" ; \
		else \
		  echo ">>> W: possibly bad URL $$f" ; \
		  echo ">>> W: possibly bad URL $$f" >> $(DRAW)/headerurl.rawxml.urlsbad ; \
		fi; \
		done; echo

# Even with user-agent spoofing, followings fail but these are good URLs
#
#  (errors happen with "--head")
#       >>> W: possibly bad URL https://tldp.org/pub/Linux/docs/ldp-archived/users-guide/
#       >>> W: possibly bad URL https://unix.org/
#       >>> W: possibly bad URL https://unix.org/version2/
#       >>> W: possibly bad URL https://unix.org/version3/
#       >>> W: possibly bad URL https://unix.org/version4/
#
#  (errors happen with "-r 0-0")
#       >>> W: possibly bad URL https://unix.org/
#       >>> W: possibly bad URL https://unix.org/version2/
#       >>> W: possibly bad URL https://unix.org/version3/
#       >>> W: possibly bad URL https://unix.org/version4/
#       >>> W: possibly bad URL https://www.netfilter.org/documentation/
#       >>> W: possibly bad URL https://www.netfilter.org/documentation/HOWTO/NAT-HOWTO.html
#       >>> W: possibly bad URL https://www.netfilter.org/documentation/HOWTO/networking-concepts-HOWTO.html
#       >>> W: possibly bad URL https://www.netfilter.org/documentation/HOWTO/packet-filtering-HOWTO.html


# Make updated en.wikipedia URL list entity (DRAW)/header.rawxml
$(DRAW)/header.rawxml: $(DRAW)/header1.rawxml $(DRAW)/header2.rawxml $(DRAW)/headerurl.rawxml
	cat $(DRAW)/header1.rawxml >  $@
	fgrep -e ' "https://en.wikipedia.org/' $(DRAW)/headerurl.rawxml >> $@
	cat $(DRAW)/header2.rawxml >> $@

# GENERATE
# source XML inclusion file updated for normal build (no remote access)
common.ent: Makefile debian/changelog $(DRAW)/headerurl.rawxml
	# Update parameters
	echo "<!ENTITY build-date \"$(SOURCE_DATE)\">"			>  $@
	echo "<!ENTITY dr-version \"$(DR_VERSION)\">"			>> $@
	echo "<!ENTITY arch \"$(ARCH)\">"				>> $@
	echo "<!ENTITY codename-stable  \"$(RELEASE_NAME0L)\">"		>> $@
	echo "<!ENTITY Codename-stable  \"$(RELEASE_NAME0U)\">"		>> $@
	echo "<!ENTITY codename-testing  \"$(RELEASE_NAME1L)\">"	>> $@
	echo "<!ENTITY Codename-testing  \"$(RELEASE_NAME1U)\">"	>> $@
	echo "<!ENTITY codename-nexttesting  \"$(RELEASE_NAME2L)\">"	>> $@
	echo "<!ENTITY Codename-nexttesting  \"$(RELEASE_NAME2U)\">"	>> $@
	echo "<!ENTITY codename-unstable \"sid\">"			>> $@
	echo "<!ENTITY Codename-unstable \"Sid\">"                      >> $@
	echo "<!ENTITY release-outdated  \"$(RELEASE_NAMEXX)\">"	>> $@
	echo "<!ENTITY Codename-doc-1    \"$(RELEASE_DOC_1)\">"		>> $@
	echo "<!ENTITY Codename-doc-2    \"$(RELEASE_DOC_2)\">"		>> $@
	# Don't trancelate non-wikipedia URL links
	fgrep -v -e ' "https://en.wikipedia.org/' $(DRAW)/headerurl.rawxml  >> $@

# Replace table contents with @-@popcon*@-@ and @@@psize*@-@ and
# fix URL referencees and table ID.
$(MANUAL).en.xml: $(DRAW)/$(MANUAL).rawxml $(DRAW)/header.rawxml
	@$(call check-command, xsltproc, xsltproc)
	$(XPNO) $(DXSL)/table.xsl $(DRAW)/$(MANUAL).rawxml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r $(DRAW)/header.rawxml" |\
	sed -e 's/@-@amp@-@/\&/g' \
	    -e 's/@-@XML_COMMENT_START@-@/$(XML_COMMENT_START)/g' \
	    -e 's/@-@XML_COMMENT_END@-@/$(XML_COMMENT_END)/g' \
	    -e 's/@-@\([^@]\+\)@-@/\&\1;/g' \
	    > $@

# Replace table contents with dummy text and
# fix URL references and table ID as the template for translation.
# This avoids bloated PO/POT files. (tablet.xsl used instead of table.xsl)
$(MANUAL).en.xmlt: $(DRAW)/$(MANUAL).rawxml $(DRAW)/header.rawxml
	# GENERATE $(MANUAL).en.xmlt (TEMPLATE to avoid bloated PO/POT files)
	@$(call check-command, xsltproc, xsltproc)
	# Note: tablet.xsl with tailing "t"
	$(XPNO) $(DXSL)/tablet.xsl $(DRAW)/$(MANUAL).rawxml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r $(DRAW)/header.rawxml" |\
	sed -e 's/@-@amp@-@/\&/g' \
	    -e 's/@-@XML_COMMENT_START@-@/$(XML_COMMENT_START)/g' \
	    -e 's/@-@XML_COMMENT_END@-@/$(XML_COMMENT_END)/g' \
	    -e 's/@-@\([^@]\+\)@-@/\&\1;/g' \
	    > $@

#######################################################################
# Phase 3 build : English XML -> POT/PO/XML non-ENGLISH
#######################################################################
# This phase of build now calls po4a twice via $(PO4A1) and $(PO4A2)
# base XML files for all languages and update PO
# Completion marker is fuzzy1.log and fuzzy2.log for translation status

POFILES = $(addsuffix .po, $(addprefix  po/, $(LANGPO)))

fuzzy1.log: $(POFILES) $(MANUAL).en.xml $(MANUAL).en.xmlt
	@$(call check-command, po4a)
	@$(call check-command, msgcat, gettext)
	$(PO4A1) $(LANGPO)
	$(POFUZZY1) $(LANGPO) >fuzzy1.log

.PHONY: xml
fuzzy2.log: fuzzy1.log
	$(PO4ATMP) $(LANGPO)
	$(POFUZZY2) $(LANGPO) >fuzzy2.log
	cp -r po-tmp po-keep
	$(PO4A2) $(LANGPO)

.PHONY: po
po: fuzzy1.log

.PHONY: xml
xml: fuzzy2.log

$(addsuffix .xml, $(addprefix  $(MANUAL)., $(LANGPO))): xml

#######################################################################
# Phase 4 build : Formatted conversion from XML
#######################################################################

#######################################################################
# $ make css       # update CSS and DIMG in $(BUILD_DIR)
#######################################################################
.PHONY: css
css:
	-rm -rf $(BUILD_DIR)/images
	mkdir -p $(BUILD_DIR)/images
	cp -f $(DXSL)/$(MANUAL).css $(BUILD_DIR)/$(MANUAL).css
	echo "AddCharset UTF-8 .txt" > $(BUILD_DIR)/.htaccess
	#cd $(DIMG) ; cp caution.png home.png important.png next.png note.png prev.png tip.png up.gif warning.png $(BUILD_DIR)/images
	cd $(DIMG) ; cp caution.png important.png note.png tip.png up.gif warning.png $(BUILD_DIR)/images
	cd png ; cp home.png next.png prev.png $(BUILD_DIR)/images

#######################################################################
# $ make html      # update all HTML in $(BUILD_DIR)
#######################################################################
.PHONY: html
html:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/index.$(LX).html)

$(BUILD_DIR)/index.%.html: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	$(XPINC)   --stringparam base.dir $(BUILD_DIR)/ \
                --stringparam html.ext .$*.html \
                $(DXSL)/style-html.xsl $<

#######################################################################
# $ make txt       # update all Plain TEXT in $(BUILD_DIR)
#######################################################################
.PHONY: txt
txt:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).txt.gz)

# style-txt.xsl provides work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.txt.gz: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, w3m, w3m)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	@test -n /usr/bin/w3m  || { echo "ERROR: w3m not found. Please install the w3m package." ; false ;  }
	$(XPINC) $(DXSL)/style-txt.xsl $< | LC_ALL=en_US.UTF-8 \
	  w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html | \
	  gzip -n -9 - > $@

#######################################################################
# $ make pdf       # update all PDF in $(BUILD_DIR)
#######################################################################
.PHONY: pdf
pdf:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).pdf)

ifeq "$(STRATEGY)" "stable"
nopdf: $(BUILD_DIR)/nopdf.pdf

$(BUILD_DIR)/nopdf.pdf: nopdf.tex
	-mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR); \
	xelatex ../nopdf.tex

$(foreach LX, $(NOPDF), $(BUILD_DIR)/$(MANUAL).$(LX).pdf): $(BUILD_DIR)/nopdf.pdf
	-mkdir -p $(BUILD_DIR)
	cp $(BUILD_DIR)/nopdf.pdf $@

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.pdf: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, dblatex, dblatex)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	@test -n "$(DBLATEX)"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { \
		echo "Error message here may be ignored as long as PDF(language=$*) file is generated."; \
		if test -e $@ ; then \
	             echo "PDF($@) build successfully"; true ; \
	        else \
		     echo "PDF($@) is missing"; false ; \
		fi ; }

$(KEEP_DIR)/$(MANUAL).%.pdf: $(BUILD_DIR)/$(MANUAL).%.pdf
	cp -f $(BUILD_DIR)/$(MANUAL).$*.pdf $(KEEP_DIR)/$(MANUAL).$*.pdf

.PHONY: copy
copy:
	$(MAKE)  $(foreach LX, $(LANGALL), $(KEEP_DIR)/$(MANUAL).$(LX).pdf)

.PHONY: prep
prep:
	mkdir -p $(KEEP_DIR)
	git describe > $(KEEP_DIR)/git_id
	$(MAKE) copy "LANGPO=$(LANGPO_PREP)"

else # STRATEGY "unstable"

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.pdf: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, dblatex, dblatex)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	@test -n "$(DBLATEX)"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { \
		echo "Error message here may be ignored as long as PDF(language=$*) file is generated."; \
		if test -e $@ ; then \
	             echo "PDF($@) build successfully"; true ; \
		elif git diff --quiet $(shell cat $(KEEP_DIR)/git_id)..HEAD po rawxml && test -e $(KEEP_DIR)/$(MANUAL).$*.pdf; then \
	        cp -f $(KEEP_DIR)/$(MANUAL).$*.pdf $(BUILD_DIR)/$(MANUAL).$*.pdf ; true ;\
		else \
		     echo "valid PDF($(KEEP_DIR)/$(MANUAL).$*.pdf) is missing"; false ; \
		fi ; }
endif

#######################################################################
# $ make tex       # update all TeX source in $(BUILD_DIR)
#######################################################################
.PHONY: tex
tex:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).tex)

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.tex: $(MANUAL).%.xml $(SRC_ENT)
	-mkdir -p $(BUILD_DIR)
	@test -n "$(DBLATEX)"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--type=tex \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { \
		echo "Error message here may be ignored as long as PDF(language=$*) file is generated."; \
		if test -e $@ ; then \
	             echo "tex source ($@) build successfully"; true ; \
		else \
		     echo "tex source ($@) is missing"; false ; \
		fi }

#######################################################################
# $ make epub      # update all epub in $(BUILD_DIR)
#######################################################################
.PHONY: epub
epub:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).epub)

$(BUILD_DIR)/$(MANUAL).%.epub: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)/$*/
	cd $(BUILD_DIR)/$*/ ; $(XPINC) $(CURDIR)/$(DXSL)/style-epub.xsl $(CURDIR)/$<
	cp -f $(DXSL)/mimetype $(BUILD_DIR)/$*/mimetype
	cp -f $(DXSL)/$(MANUAL).css $(BUILD_DIR)/$*/OEBPS/$(MANUAL).css
	cp -f $(DXSL)/debian-openlogo.png $(BUILD_DIR)/$*/OEBPS/debian-openlogo.png
	find  $(BUILD_DIR)/$*/ | xargs -n1 touch -d"$(SOURCE_DATE)"
	cd $(BUILD_DIR)/$*/ ; $(ZIP) --latest-time -r $@ ./

#######################################################################
# Phase 5 cleans
#######################################################################
#######################################################################
# $ make clean     # clean files ready for tar
#                  (override_dh_auto_clean call this)
#######################################################################
.PHONY: clean
clean: localclean remoteclean
.PHONY: distclean
distclean: clean
#######################################################################
# $ make localclean   # clean locally generated files
#######################################################################
.PHONY: localclean
localclean:
	# CLEAN
	-rm -rf $(BUILD_DIR)
	-rm -rf po-tmp po-keep
	-rm -rf debian/tmp
	-rm -f *.swp *~ *.tmp
	-rm -f $(DPO)/*~ $(DPO)/*.mo $(DPO)/*.po.*
	-rm -f common.ent
	-rm -f pkg.lst
	-rm -f $(addsuffix .xml, $(addprefix $(MANUAL)., *))
	-rm -f $(addsuffix .xmlt, $(addprefix $(MANUAL)., *))
	-rm -f $(DRAW)/$(MANUAL).rawxml
	-rm -f $(DRAW)/headerurl.rawxml
	-rm -f $(DRAW)/headerurl.rawxml.dups
	-rm -f $(DRAW)/headerurl.rawxml.entdups
	-rm -f $(DRAW)/headerurl.rawxml.http
	-rm -f $(DRAW)/headerurl.rawxml.https
	-rm -f $(DRAW)/headerurl.rawxml.urls
	-rm -f $(DRAW)/headerurl.rawxml.urlsbad
	-rm -f $(DRAW)/header.rawxml
	-rm -f fuzzy*.log
	-rm -f all-xml.stamp
	-rm -f po4a.cfg po4a-tmp.cfg

#######################################################################
# $ make remoteclean # clean remote data to generate entity files
#######################################################################
.PHONY: remoteclean
remoteclean:
	-rm -f $(REMOTE_DATA)

#######################################################################
# $ make entityclean # clean entity files generated from remote data
#######################################################################
.PHONY: entityclean
entityclean:
	-rm -f remote.ent

#======================================================================
# ===== Utility targets ===============================================
#======================================================================

#######################################################################
# $ make wrap       # wrap all PO
#######################################################################
.PHONY: wrap wrapx nowrap nowrapx unwrap wip replace check stat
wrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX ;\
	done

#$(MSGCAT) -o $(DPO)/$(MANUAL).pot $(DPO)/$(MANUAL).pot

unwrap: nowrap

nowrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	echo "I: $$XX FUZZY=$$(grep -e '^#,.*fuzzy' $$XX|wc -l) BLANK=$$(grep -e '^msgstr \"\"' $$XX|wc -l)" ;\
	done

#$(MSGCAT) -o $(DPO)/$(MANUAL).pot  --no-wrap $(DPO)/$(MANUAL).pot

# remove obsolete
wrapx:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) --no-obsolete -o $$XX $$XX ;\
	done

# remove obsolete
nowrapx:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) --no-wrap --no-obsolete -o $$XX $$XX ;\
	done

# "make nowrap; make wrap" produces original good POs of "make po" but not for POT

# For global "sed" for RAWXMLs and POs, edit and use bin/sedxmlpo

# use sed to replace text with $(DBIN)/replace0
replace:
	@$(call check-file, $(SEDF))
	@$(call check-command, msgcat, gettext)
	@$(call check-command, msguniq, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	done
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	cat $$XX >$$XX.sed ;\
	$(SED) -f $(SEDF) <$$XX | sed -n -e '/^msgid "[^"]/,$$p' >>$$XX.sed ;\
	$(MSGUNIQ) -o $$XX.uniq $$XX.sed ;\
	done
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX.uniq ;\
	done

wip:
	@$(call check-command, msgattrib, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) -o $$XX.fuzz --fuzzy        $$XX ;\
	$(MSGATTR) -o $$XX.untr --untranslated $$XX ;\
	done

check:
	# check for po file syntax such as "
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) $$XX >/dev/null ;\
	done
	# check for URL string with %
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT)  --no-wrap $$XX | grep -e '^msgstr "http.*%' || true ;\
	done

stat:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	echo "I: $$XX --> FUZZY+BLANK=$$($(MSGCAT) --no-wrap $$XX | grep -e '^#,.*fuzzy' -e '^msgstr \"\"' |wc -l)" ;\
	done

#######################################################################
# $ make rsync
# export build result to http://people.debian.org/~osamu/$(MANUAL)/
#######################################################################
.PHONY: rsync
rsync: all
	rsync -avz $(BUILD_DIR)/ osamu@people.debian.org:public_html/$(MANUAL)/

#######################################################################
# $ make sbuild       # check build in sbuld
#######################################################################
.PHONY: sbuild
# ~/.sbuildrc can be linked to ~/.sbuildrc-src or others
# Just use this to test build locally
sbuild:
	# update chroot with my local command
	echo "Did I run 'upu'?"
	# build in chroot
	sbuild
#######################################################################
# $ make dgit       # upload with dgit
#######################################################################
.PHONY: dgit
# Source only upload as native package per dgit-maint-native(7)
# Local build results are irrerevant for upload
dgit:
	dgit -wgf push-source

